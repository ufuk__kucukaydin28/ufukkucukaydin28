﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vize_NOtu_Ödevi
{
    class Vize_Final_Notu
    {
        public string ad;
        public string soyad;
        public string ders;
        private int vize;
        private int final;
        private string tc;
        public string Tc

        {
            get { return tc; }

            set
            {
                if (value.Length == 11)
                {
                    value = tc;
                }
                else
                {
                    Console.WriteLine("HATALI SAYI GİRİŞİ YAPTINIZ");
                }
            }
            
        }
        public int Vize  
        {
            get { return vize; }
            set
            {
                if (value< 0 && value > 100)
                {
                    value = vize;
                }
                else
                {
                    Console.WriteLine("HATALI SAYI GİRİŞİ YAPTINIZ");
                }
            }
            
        }
        public int Final  
        {
            get { return Final; }
            set
            {
                if (value < 0 && value > 100)
                {
                    value = final;
                }
                else
                {
                    Console.WriteLine("HATALI SAYI GİRİŞİ YAPTINIZ");
                }
            }
        }

        public int final_notu (int f )
        {
            return (f* 40) / 100;
        }
        public int vize_notu (int v)
        {
            return (v * 60) / 100;
        }
    }
}
